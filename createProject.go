package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	projectv1 "github.com/openshift/client-go/project/clientset/versioned/typed/project/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	clientcmd "k8s.io/client-go/tools/clientcmd"
	v1 "github.com/openshift/api/project/v1"
)
func main {
	// Load the kube config file
	var kubeconfig *string
	kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	flag.Parse()
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		fmt.Fprintln("Issue while loading kube file %v", err)
		panic(err)
	}
	projectV1Client, err := projectv1.NewForConfig(config)
	if err != nil {
		fmt.Fprintln("Issue while getting project object %v", err)
		panic(err)
	}
	projectRequest := v1.ProjectRequest{}
	projectRequest.Kind = "ProjectRequest"
	projectRequest.APIVersion = "project.openshift.io/v1"
	objectMeta := metav1.ObjectMeta{}
	objectMeta.Name = "test"
	projectRequest.ObjectMeta = objectMeta
	projectRequest.DisplayName = "test project"
	projectRequest.Description = "This is a test project"
	newProject, err := projectV1Client.ProjectRequests().Create(&projectRequest)
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
}