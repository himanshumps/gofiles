package main

import (
	"flag"
	"fmt"
	templatev1client "github.com/openshift/client-go/template/clientset/versioned/typed/template/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	clientcmd "k8s.io/client-go/tools/clientcmd"
	//templatev1 "github.com/openshift/api/template/v1"
	"github.com/davecgh/go-spew/spew"
)
func main() {
	// Load the kube config file
	namespace: "test"
	kubeconfig := flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	flag.Parse()
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		fmt.Println("Issue while loading kube file %v", err)
		panic(err)
	}
	
	templateclient, err := templatev1client.NewForConfig(config)
	if err != nil {
		fmt.Println("Issue while getting template object %v", err)
		panic(err)
	}
	template, err := templateclient.Templates("openshift").Get("postgresql-persistent", metav1.GetOptions{})
	spew.Dump(template)
}